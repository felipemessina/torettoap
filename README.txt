#######################################################################################################################
########################################       INFORMATIONS       #####################################################
#######################################################################################################################

This is an e-commerce website, and it was designed to help Toretto Auto Parts provide its customers a place where they can buy car auto parts without leaving their houses. 
---------------------
#ABOUT
This project was built using the following technologies:
Java 1.6
JSF Mojarra 2.1.6
jUnit 4
Tomcat 7
Primefaces 5.1
---------------------
#OTHERS JARS
el-ri-1.0.jar
jstl-1.2.jar
---------------------
#TO RUN THE TESTS
Run the classes ManageCartTest and ManageProductTest
---------------------
#USING THE APPLICATION

To run the app, please follow the steps:
1) Start Tomcat
2) Go to the following URL: http://localhost:8080/TorettoAP/
3) Start shopping

Any problem, contact: felipe.messina@gmail.com