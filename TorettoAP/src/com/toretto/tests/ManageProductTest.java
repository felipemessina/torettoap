package com.toretto.tests;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.toretto.br.ManageProduct;
import com.toretto.to.Product;

public class ManageProductTest {
	
	private ManageProduct manageProduct;
	private Product product;
	
	
	@Before
	public void initTests(){
		manageProduct = new ManageProduct();
		product = new Product();
	}
	
	@Test
	public void checkStockOfProductNotAvailable(){
		product.setId("05000NOS");
		assertEquals("0", manageProduct.checkProductStock(product).toString());
	}
	
	@Test
	public void checkStockOf8ProductsAvailable(){
		product.setId("DK37");
		assertEquals("8", manageProduct.checkProductStock(product).toString());
	}
	
	@Test
	public void removeOneProductOfStock(){
		product.setId("F1G1FA");
		int positionInList = 3;
		assertEquals("1", manageProduct.adjustProductStock(product, Boolean.TRUE, null).get(positionInList).getStockQtt().toString());
	}
	
	@Test
	public void removeOneProductOfStockAndAddAgain(){
		product.setId("715A");
		int positionInList = 0;
		assertEquals("9", manageProduct.adjustProductStock(product, Boolean.TRUE, null).get(positionInList).getStockQtt().toString());
		assertEquals("10", manageProduct.adjustProductStock(product, Boolean.FALSE, 1).get(positionInList).getStockQtt().toString());
	}

}
