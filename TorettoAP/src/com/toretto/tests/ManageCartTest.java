package com.toretto.tests;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;

import com.toretto.br.ManageCart;
import com.toretto.to.Product;
import com.toretto.to.ShoppingCart;

public class ManageCartTest {
	
	private ManageCart manageCart;
	
	@Before
	public void initTests(){
		manageCart = new ManageCart();
	}
	
	
	@Test
	public void checkIfAddProductInCart(){
		Product product = new Product();
		product.setId("715A");
		product.setName("Spartan/ATK Engines Spartan Remanufactured Subaru Engine");
		product.setStockQtt(10);
		product.setValue(new BigDecimal("2.000"));
		product.setImagePath("/images/products/"+product.getId()+".jpg");
		product.setDescription("Remanufactured Engines are completely disassembled & put through the same process every time. All heads, blocks, cranks and cams are machined & wear parts are replaced. Each engine is individually tested after assembly to assure compression, oil pressure and water jacket integrity.");
		
		assertEquals(new BigDecimal("2.000"), manageCart.addOrder(product, new ShoppingCart()).getTotalValue());
		
	}
	
	@Test
	public void checkIfAdd2ProductsInCart(){
		Product product = new Product();
		product.setId("715A");
		product.setName("Spartan/ATK Engines Spartan Remanufactured Subaru Engine");
		product.setStockQtt(10);
		product.setValue(new BigDecimal("2.000"));
		product.setImagePath("/images/products/"+product.getId()+".jpg");
		product.setDescription("Remanufactured Engines are completely disassembled & put through the same process every time. All heads, blocks, cranks and cams are machined & wear parts are replaced. Each engine is individually tested after assembly to assure compression, oil pressure and water jacket integrity.");
		
		ShoppingCart cart =  manageCart.addOrder(product, new ShoppingCart());
		assertEquals(new BigDecimal("4.000"), manageCart.addOrder(product, cart).getTotalValue());
		
	}
	
	@Test
	public void checkIfAddAndRemoveAProductFromCart(){
		Product product = new Product();
		product.setId("715A");
		product.setName("Spartan/ATK Engines Spartan Remanufactured Subaru Engine");
		product.setStockQtt(10);
		product.setValue(new BigDecimal("2.000"));
		product.setImagePath("/images/products/"+product.getId()+".jpg");
		product.setDescription("Remanufactured Engines are completely disassembled & put through the same process every time. All heads, blocks, cranks and cams are machined & wear parts are replaced. Each engine is individually tested after assembly to assure compression, oil pressure and water jacket integrity.");
		
		ShoppingCart cart =  manageCart.addOrder(product, new ShoppingCart());
		assertEquals(new BigDecimal("2.000"),cart.getTotalValue());
		
		assertEquals(new BigDecimal("0"), manageCart.removeOrder(product, 1, cart).getTotalValue());
		
	}
	
	

}
