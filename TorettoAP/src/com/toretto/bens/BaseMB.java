package com.toretto.bens;

import java.io.Serializable;

import javax.el.ELContext;
import javax.faces.FacesException;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

/**
 * 
 * @name BaseMB
 * 
 * @author Felipe Messina
 * @email felipe.messina@gmail.com
 * @descripton Base Managed Bean with the commom methods used on the project
 *
 */
public class BaseMB implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6300559216124529683L;

	protected FacesContext getFacesContext() {
		return FacesContext.getCurrentInstance();
	}

	protected HttpServletRequest getRequest() {
		return (HttpServletRequest) getFacesContext().getExternalContext()
				.getRequest();
	}
	
	public Object getManagedBean(String nomeManagedBean) {
		Object managedBean = null;
		try {
			ELContext elContext = FacesContext.getCurrentInstance().getELContext();
			managedBean = elContext.getELResolver().getValue(elContext, null, nomeManagedBean);
		} catch (RuntimeException ex) {
			throw new FacesException(ex.getMessage(), ex);
		}
		return managedBean;
	}
	
	public void addInfoMessage(String field, String summary, String detail ){
		FacesContext.getCurrentInstance().addMessage(field, new FacesMessage(FacesMessage.SEVERITY_INFO, summary , detail));
	}
	
	public void addErrorMessage(String field, String summary, String detail ){
		FacesContext.getCurrentInstance().addMessage(field, new FacesMessage(FacesMessage.SEVERITY_ERROR, summary , detail));
	}
	
}
