package com.toretto.bens;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.toretto.br.ManageCart;
import com.toretto.br.ManageProduct;
import com.toretto.to.Product;
import com.toretto.to.ShoppingCart;
import com.toretto.utils.Constants;

@ManagedBean
@SessionScoped
public class ShoppingCartMB extends BaseMB{

	private static final long serialVersionUID = 8046106719825164546L;

	private ManageCart manageCart = new ManageCart();
	private ShoppingCart cart = new ShoppingCart();
	private ManageProduct manageProduct = new ManageProduct();
	
	public void addProductInCart(Product product){
		try{
			if(manageProduct.checkProductStock(product) > 0){
				cart = manageCart.addOrder(product, cart);
				addInfoMessage(null, null, Constants.MSG_INFO_PRODUCT_ADD);
			}else{
				addErrorMessage(null, null, Constants.MSG_PRODUCT_NOT_AVAILABLE );
			}
		}catch(Exception e){
			addErrorMessage(null, null, Constants.MSG_ERROR_PRODUCT_ADD );
		}
	}
	
	public void removeProductFromCart(Product product, Integer qttRemove){
		try{
			cart = manageCart.removeOrder(product, qttRemove, cart);
			addInfoMessage(null, null, Constants.MSG_INFO_PRODUCT_REMOVE  );
		}catch(Exception e){
			addErrorMessage(null, null, Constants.MSG_ERROR_PRODUCT_REMOVE  );
		}
	}
	
	public ShoppingCart getCart() {
		return cart;
	}
	
	public void setCart(ShoppingCart cart) {
		this.cart = cart;
	}

	public ManageCart getManageCart() {
		return manageCart;
	}

}
