package com.toretto.bens;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.toretto.br.ManageProduct;
import com.toretto.to.Product;
import com.toretto.utils.Constants;

@ManagedBean
@ViewScoped
public class ProductDetailsMB extends BaseMB{

	private static final long serialVersionUID = 740221533854999950L;
	
	private Product product = new Product();
	private ManageProduct manageProduct = new ManageProduct();
	
	@PostConstruct
	public void init(){
		product = manageProduct.refreshProduct((Product) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("prod"));  
	}
	
	public void addProductInCart(Product pProduct){
		try{
			
			if(manageProduct.checkProductStock(pProduct) > 0){
				ShoppingCartMB mb = (ShoppingCartMB) getManagedBean("shoppingCartMB");
				
				mb.setCart(mb.getManageCart().addOrder(pProduct, mb.getCart())); 
				addInfoMessage(null, null, Constants.MSG_INFO_PRODUCT_ADD );
			
				product = manageProduct.refreshProduct(pProduct);
				
			}else{
				addErrorMessage(null, null, Constants.MSG_PRODUCT_NOT_AVAILABLE );
			}
			
		}catch(Exception e){
			addErrorMessage(null, null, Constants.MSG_ERROR_PRODUCT_ADD );
		}
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}
}
