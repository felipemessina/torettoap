package com.toretto.bens;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.toretto.br.ManageProduct;
import com.toretto.to.Product;
import com.toretto.utils.Constants;
import com.toretto.utils.TorettoAPUtils;

@ManagedBean
@ViewScoped
public class ProductBrowserMB extends BaseMB{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4744831967211117978L;
	
	ManageProduct manageProduct = new ManageProduct();
	List<Product> listProducts = new ArrayList<Product>();
	
	@PostConstruct
	public void init(){
		listProducts = TorettoAPUtils.productList;
	}
	
	public void addProductInCart(Product product){
		try{
			if(manageProduct.checkProductStock(product) > 0){
				ShoppingCartMB mb = (ShoppingCartMB) getManagedBean("shoppingCartMB");
				
				mb.setCart(mb.getManageCart().addOrder(product, mb.getCart())); 
				listProducts = TorettoAPUtils.productList;
				addInfoMessage(null, null, Constants.MSG_INFO_PRODUCT_ADD );
			}else{
				addErrorMessage(null, null, Constants.MSG_PRODUCT_NOT_AVAILABLE );
			}
		}catch(Exception e){
			addErrorMessage(null, null, Constants.MSG_ERROR_PRODUCT_ADD );
		}
	}
	
	
	public String showProductDetails(Product prod){
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("prod", prod);
		return "productDetails?faces-redirect=true";
	}
	
	public List<Product> getListProducts() {
		return listProducts;
	}

}
