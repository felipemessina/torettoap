package com.toretto.br;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.toretto.to.Order;
import com.toretto.to.Product;
import com.toretto.to.ShoppingCart;


public class ManageCart implements Serializable {
	
	private static final long serialVersionUID = 842415384750009996L;
	
	ManageProduct manageProduct = new ManageProduct();
	
	public ShoppingCart addOrder(Product product, ShoppingCart cart){
		ShoppingCart cartAux = new ShoppingCart();
		Order order = new Order();
		order.setProductQtt(1);
		
		if(cart.getOrders() != null && cart.getOrders().size() > 0){
			for (Order orderAux : cart.getOrders()) {
				int index = cart.getOrders().indexOf(orderAux);
				if(orderAux.getProduct().getId().equals(product.getId())){
					order.setProductQtt(cart.getOrders().get(index).getProductQtt() +1);
					order.setProduct(orderAux.getProduct());
					cartAux.getOrders().add(order);
				}else{
					cartAux.getOrders().add(orderAux);
				}
			}
		}
		if(order.getProductQtt() == 1){
			order.setProduct(product);
			cartAux.getOrders().add(order);
		}
		cartAux.setTotalValue(adjustCartValue(cartAux));
		manageProduct.adjustProductStock(product, Boolean.TRUE, null);
		return cartAux;
	}
	
	public ShoppingCart removeOrder(Product product, Integer qttRemove, ShoppingCart cart){
		ShoppingCart cartAux = new ShoppingCart();
		List<Order> orders = new ArrayList<Order>();
		
		for (Order orderAux : cart.getOrders()) {
			if(orderAux.getProduct().getId().equals(product.getId())){
				
				orderAux.setProductQtt(orderAux.getProductQtt() - qttRemove);
				
				if(orderAux.getProductQtt() > 0)
					orders.add(orderAux);
			
			}else{
				orders.add(orderAux);
			}
			
		}
		cartAux.setOrders(orders);
		
		manageProduct.adjustProductStock(product, Boolean.FALSE, qttRemove);
		cartAux.setTotalValue(adjustCartValue(cartAux));
		return cartAux;
	}
	
	private BigDecimal adjustCartValue(ShoppingCart cart){
		BigDecimal cartValue = BigDecimal.ZERO;
		
		for (Order order : cart.getOrders()) {
			cartValue = cartValue.add(order.getTotalOrderValue());
		}
		return cartValue;
	}
	
}
