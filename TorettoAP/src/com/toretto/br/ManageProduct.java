package com.toretto.br;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.toretto.to.Product;
import com.toretto.utils.TorettoAPUtils;

public class ManageProduct implements Serializable {
	
	private static final long serialVersionUID = 7145037158737161279L;

	public Integer checkProductStock(Product product){
		int index = TorettoAPUtils.productList.indexOf(product);
		return TorettoAPUtils.productList.get(index).getStockQtt();
	}
	
	public List<Product> adjustProductStock(Product product, Boolean addProduct, Integer qtt){
		List<Product> listAux = new ArrayList<Product>();
		for (Product productAux : TorettoAPUtils.productList) {
			if(productAux.getId().equals(product.getId())){
				if(addProduct){
					productAux.setStockQtt(productAux.getStockQtt()-1);
				}else{
					productAux.setStockQtt(productAux.getStockQtt()+ qtt );
				}
				listAux.add(productAux);
			}else{
				listAux.add(productAux);
			}
		}
		return listAux;
	}
	
	public Product refreshProduct(Product product){
		if(product != null){
			for (Product prod : TorettoAPUtils.productList) {
				if(prod.getId().equals(product.getId())){
					return prod;
				}
			}
		}
		return new Product();
	}
}
