package com.toretto.to;

import java.io.Serializable;
import java.math.BigDecimal;

public class Order implements Serializable{
	
	private static final long serialVersionUID = -3187233300569753472L;
	
	private Integer productQtt;
	private Product product;
	private BigDecimal totalOrderValue;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((product == null) ? 0 : product.hashCode());
		result = prime * result
				+ ((productQtt == null) ? 0 : productQtt.hashCode());
		result = prime * result
				+ ((totalOrderValue == null) ? 0 : totalOrderValue.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Order other = (Order) obj;
		if (product == null) {
			if (other.product != null)
				return false;
		} else if (!product.equals(other.product))
			return false;
		if (productQtt == null) {
			if (other.productQtt != null)
				return false;
		} else if (!productQtt.equals(other.productQtt))
			return false;
		if (totalOrderValue == null) {
			if (other.totalOrderValue != null)
				return false;
		} else if (!totalOrderValue.equals(other.totalOrderValue))
			return false;
		return true;
	}
	
	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Integer getProductQtt() {
		return productQtt;
	}

	public void setProductQtt(Integer productQtt) {
		this.productQtt = productQtt;
	}

	public BigDecimal getTotalOrderValue() {
		return product.getValue().multiply(new BigDecimal(productQtt));
	}

	public void setTotalOrderValue(BigDecimal totalOrderValue) {
		this.totalOrderValue = totalOrderValue;
	}
	
}