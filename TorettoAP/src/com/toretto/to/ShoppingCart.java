package com.toretto.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class ShoppingCart implements Serializable{
	
	private static final long serialVersionUID = -8124871089869788516L;

	private List<Order> orders;
	private BigDecimal totalValue;
	
	public ShoppingCart() {
		this.orders = new ArrayList<Order>();
		this.totalValue = BigDecimal.ZERO;
	}
	
	public List<Order> getOrders() {
		return orders;
	}
	public void setOrders(List<Order> orders) {
		this.orders = orders;
	}
	public BigDecimal getTotalValue() {
		return totalValue;
	}
	public void setTotalValue(BigDecimal totalValue) {
		this.totalValue = totalValue;
	}
	
}
