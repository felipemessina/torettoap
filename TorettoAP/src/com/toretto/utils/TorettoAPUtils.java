package com.toretto.utils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.toretto.to.Product;

public class TorettoAPUtils {
	
	public static List<Product> productList = new ArrayList<Product>(){{
		Product product = new Product();
		
		product.setId("715A");
		product.setName("Spartan/ATK Engines Spartan Remanufactured Subaru Engine");
		product.setStockQtt(10);
		product.setValue(new BigDecimal("2000"));
		product.setImagePath("/resources/images/products/"+product.getId()+".jpg");
		product.setDescription("Remanufactured Engines are completely disassembled & put through the same process every time. All heads, blocks, cranks and cams are machined & wear parts are replaced. Each engine is individually tested after assembly to assure compression, oil pressure and water jacket integrity.");
		add(product);
		product = new Product();
		
		product.setId("DK37");
		product.setName("Spartan/ATK Engines Spartan Remanufactured Cadillac Engine");
		product.setStockQtt(8);
		product.setValue(new BigDecimal("1400"));
		product.setImagePath("/resources/images/products/"+product.getId()+".jpg");
		product.setDescription("Remanufactured Engines are completely disassembled & put through the same process every time. All heads, blocks, cranks and cams are machined & wear parts are replaced. Each engine is individually tested after assembly to assure compression, oil pressure and water jacket integrity.");
		add(product);
		product = new Product();
		
		product.setId("05000NOS");
		product.setName("NOS/Nitrous Oxide Systems Powershot; Nitrous System");
		product.setStockQtt(0);
		product.setValue(new BigDecimal("900"));
		product.setImagePath("/resources/images/products/"+product.getId()+".jpg");
		product.setDescription("Easy To Install / Thin Injector Plate- 0.5in. Thick / Accurate Spray Bars / Smooth Extra Power Rush / Fuel Economy Unaffected / Provides Safe Power / Adjustable Power Levels");
		add(product);
		product = new Product();
		
		product.setId("F1G1FA");
		product.setName("Spectra Premium Fuel Pump and Tank Assembly Includes Fuel Tank and Mounting Straps");
		product.setStockQtt(2);
		product.setValue(new BigDecimal("125"));
		product.setImagePath("/resources/images/products/"+product.getId()+".jpg");
		product.setDescription("Innovative engineering, manufacturing technology and quality control ensure that Spectra Premium fuel tank assemblies meet or exceed the performance of the original equipment they replace.");
		add(product);
		product = new Product();
		
		product.setId("GNAD465A");
		product.setName("Wearever Gold Ceramic Brake Pads (4-Pad Set)");
		product.setStockQtt(5);
		product.setValue(new BigDecimal("190"));
		product.setImagePath("/resources/images/products/"+product.getId()+".jpg");
		product.setDescription("Wearever Gold brake pads are premium original equipment style replacement brake pads. Wearever Gold Brake Pads deliver superior stopping power and feature Advance Auto Part's exclusive multi-layer SoundLock noise dampening shim for ultra-quiet braking.");
		add(product);
		product = new Product();
		
		product.setId("4504");
		product.setName("Denso PK20TT Platinum TT Spark Plug");
		product.setStockQtt(50);
		product.setValue(new BigDecimal("10"));
		product.setImagePath("/resources/images/products/"+product.getId()+".jpg");
		product.setDescription("This spark plug features a platinum twin-tip design and a titanium enhanced ground strap.");
		add(product);
		product = new Product();
		
	}};

}
