package com.toretto.utils;

public class Constants {
	
	public static final String MSG_INFO_PRODUCT_ADD = "The product was added into the cart!";
	public static final String MSG_ERROR_PRODUCT_ADD = "Error while inserting product into the cart!";
	public static final String MSG_INFO_PRODUCT_REMOVE = "The product was removed from cart!";
	public static final String MSG_ERROR_PRODUCT_REMOVE = "Error while removing product from cart!";
	public static final String MSG_PRODUCT_NOT_AVAILABLE = "Sorry, we don't have this product in stock";

}
